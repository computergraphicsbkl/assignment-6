﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor3D
{
    public partial class MainForm : Form
    {
        private List<IPrimitive> scene = new List<IPrimitive>();
        private Transformation projection;
        private IPolyhedron curPolyhedron;
        private Graphics graphics;
        
        public MainForm()
        {
            InitializeComponent();
            Image bitmap = new Bitmap(2000, 2000);
            graphics = Graphics.FromImage(bitmap);
            pictureBox1.Image = bitmap;
            curPolyhedron = new Tetrahedron((double)numericUpDown12.Value);
            scene.Add(curPolyhedron);
            Point3D oPoint = new Point3D(0, 0, 0);
            Point3D xPoint = new Point3D(1, 0, 0, Color.Red);
            Line xAxis = new Line(oPoint, xPoint, Color.Red);
            Point3D yPoint = new Point3D(0, 1, 0, Color.LawnGreen);
            Line yAxis = new Line(oPoint, yPoint, Color.LawnGreen);
            Point3D zPoint = new Point3D(0, 0, 1, Color.Blue);
            Line zAxis = new Line(oPoint, zPoint, Color.Blue);
            scene.AddRange(new IPrimitive[] {xPoint, xAxis, yPoint, yAxis, zPoint, zAxis});
            radioButton8_CheckedChanged(this, EventArgs.Empty);
        }

        private void scenesRefresh()
        {
            graphics.Clear(Color.White);
            foreach (var primitive in scene)
                primitive.Draw(graphics, projection, pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Refresh();
        }

        private void TetrahedronButton_Click(object sender, EventArgs e)
        {
            scene.Remove(curPolyhedron);
            curPolyhedron = new Tetrahedron((double)numericUpDown12.Value);
            scene.Add(curPolyhedron);
            scenesRefresh();
        }

        private void IcosahedronButton_Click(object sender, EventArgs e)
        {
            scene.Remove(curPolyhedron);
            curPolyhedron = new Icosahedron((double)numericUpDown12.Value);
            scene.Add(curPolyhedron);
            scenesRefresh();
        }

        private void DodecahedronButton_Click(object sender, EventArgs e)
        {
            scene.Remove(curPolyhedron);
            curPolyhedron = new Dodecahedron((double)numericUpDown12.Value);
            scene.Add(curPolyhedron);
            scenesRefresh();
        }

        // apply translation
        private void button3_Click(object sender, EventArgs e)
        {
            double dx = (double)numericUpDown8.Value;
            double dy = (double)numericUpDown9.Value;
            double dz = (double)numericUpDown20.Value;
            curPolyhedron.Apply(Transformation.Translate(dx, dy, dz));
            scenesRefresh();
        }
        
        // apply rotation
        private void button2_Click(object sender, EventArgs e)
        {
            double angleX = (double)numericUpDown7.Value * Math.PI / 180;
            double angleY = (double)numericUpDown5.Value * Math.PI / 180;
            double angleZ = (double)numericUpDown6.Value * Math.PI / 180;
            curPolyhedron.Apply(Transformation.RotateX(angleX));
            curPolyhedron.Apply(Transformation.RotateY(angleY));
            curPolyhedron.Apply(Transformation.RotateZ(angleZ));
            scenesRefresh();
        }

        // apply scale
        private void button1_Click(object sender, EventArgs e)
        {
            double scaleX = (double)numericUpDown4.Value;
            double scaleY = (double)numericUpDown2.Value;
            double scaleZ = (double)numericUpDown1.Value;
            curPolyhedron.Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
            scenesRefresh();
        }

        // apply reflection
        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectX());
            }
            else if (radioButton2.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectY());
            }
            else if (radioButton3.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectZ());
            }
            scenesRefresh();
        }

        // apply center scale
        private void button7_Click(object sender, EventArgs e)
        {
            Point3D center = curPolyhedron.Center;
            double scaleX = (double)numericUpDown21.Value;
            double scaleY = (double)numericUpDown22.Value;
            double scaleZ = (double)numericUpDown23.Value;
            curPolyhedron.Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            curPolyhedron.Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
            curPolyhedron.Apply(Transformation.Translate(center.X, center.Y, center.Z));
            scenesRefresh();

        }

        // apply center rotation
        private void button5_Click(object sender, EventArgs e)
        {
            Point3D center = curPolyhedron.Center;
            double angle = (double)numericUpDown19.Value * Math.PI / 180;
            curPolyhedron.Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            if (radioButton4.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateX(angle));
            }
            else if (radioButton5.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateY(angle));
            }
            else if (radioButton6.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateZ(angle));
            }
            curPolyhedron.Apply(Transformation.Translate(center.X, center.Y, center.Z));
            scenesRefresh();
        }

        private double getAngle(double sin, double cos)
        {
            if (cos >= 0)
                return Math.Asin(sin);
            else
                return Math.PI * Math.Sign(sin) - Math.Asin(sin);
        }
        
        // apply line roll
        private void button6_Click(object sender, EventArgs e)
        {
            Line line = new Line(
                new Point3D((double) numericUpDown13.Value,
                    (double) numericUpDown14.Value,
                    (double) numericUpDown15.Value),
                new Point3D((double) numericUpDown10.Value,
                    (double) numericUpDown17.Value,
                    (double) numericUpDown18.Value));
            
            scene.Add(line);
            
            Transformation translate = Transformation.Translate(-line.A.X, -line.A.Y, -line.A.Z);
            Transformation reverseTranslate = Transformation.Translate(line.A.X, line.A.Y, line.A.Z);
            curPolyhedron.Apply(translate);
            line.Apply(translate);
            // Исключить случай, когда линия лежит на оси Oy.
            bool isOnOy = Math.Abs(line.B.X) <= double.Epsilon && Math.Abs(line.B.Z) <= double.Epsilon;
            Transformation reverseRotateY = new Transformation();
            Transformation reverseRotateZ = new Transformation();
            if (!isOnOy)
            {
                double lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Z * line.B.Z);
                double sin = line.B.Z / lineProjectionLength;
                double cos = line.B.X / lineProjectionLength;
                double lineProjectionAngle = getAngle(sin, cos);
                Transformation rotateY = Transformation.RotateY(-lineProjectionAngle);
                reverseRotateY = Transformation.RotateY(lineProjectionAngle);
                curPolyhedron.Apply(rotateY);
                line.Apply(rotateY);
                lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Y * line.B.Y);
                sin = line.B.X / lineProjectionLength;
                cos = line.B.Y / lineProjectionLength;
                lineProjectionAngle = getAngle(sin, cos);
                Transformation rotateZ = Transformation.RotateZ(-lineProjectionAngle);
                reverseRotateZ = Transformation.RotateZ(lineProjectionAngle);
                curPolyhedron.Apply(rotateZ);
                line.Apply(rotateZ);
            }
            double angle = (double) numericUpDown16.Value * Math.PI / 180;
            Transformation rotate = Transformation.RotateY(angle);
            curPolyhedron.Apply(rotate);
            if (!isOnOy)
            {
                curPolyhedron.Apply(reverseRotateZ * reverseRotateY);
                line.Apply(reverseRotateZ * reverseRotateY);
            }
            curPolyhedron.Apply(reverseTranslate);
            line.Apply(reverseTranslate);
            scenesRefresh();
            scene.Remove(line);
        }

        // perspective
        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton8.Checked)
            {
                double fov = (double)numericUpDown11.Value;
                projection = Transformation.RotateY(Math.PI / 4)
                             * Transformation.RotateX(-Math.PI / 4)
                             * Transformation.Translate(0, 0, -1)
                             * new Transformation(new double[,]
                             {
                                 {1, 0, 0, 0},
                                 {0, 1, 0, 0},
                                 {0, 0, 0, -fov},
                                 {0, 0, 0, 1}
                             });
            }
            else if (radioButton7.Checked)
                projection = Transformation.RotateY(Math.PI / 4)
                             * Transformation.RotateX(-Math.PI / 4);
            else if (radioButton9.Checked)
                projection = Transformation.Identity();
            else if (radioButton10.Checked)
                projection = Transformation.RotateY(Math.PI / 2);
            else if (radioButton11.Checked)
                projection = Transformation.RotateX(-Math.PI / 2);
            scenesRefresh();
        }

        private void pictureBox1_Resize(object sender, EventArgs e)
        {
            if (graphics != null)
                scenesRefresh();
        }

        private void numericUpDown11_ValueChanged(object sender, EventArgs e)
        {
            radioButton8_CheckedChanged(this, EventArgs.Empty);
        }
    }
}