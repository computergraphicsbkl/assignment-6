﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Editor3D
{
    interface IPrimitive
    {
        void Draw(Graphics g, Transformation projection, int width, int height);
        void Apply(Transformation t);
    }
    
    class Point3D : IPrimitive
    {
        private static double POINT_SIZE = 6;
        private double[] coords = new double[] { 0, 0, 0, 1 };
        private Color color = Color.Black;

        public double X
        {
            get => coords[0];
            set => coords[0] = value;
        }

        public double Y
        {
            get => coords[1];
            set => coords[1] = value;
        }

        public double Z
        {
            get => coords[2];
            set => coords[2] = value;
        }

        public double K
        {
            get => coords[3];
            set => coords[3] = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public Point3D() { }

        public Point3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        
        public Point3D(double x, double y, double z, Color color)
        {
            X = x;
            Y = y;
            Z = z;
            this.color = color;
        }

        private Point3D(double[] arr)
        {
            coords = arr;
        }

        public static Point3D FromPoint(Point point)
        {
            return new Point3D(point.X, point.Y, 0);
        }

        public void Apply(Transformation t)
        {
            double[] newCoords = new double[4];
            for (int i = 0; i < 4; ++i)
            {
                newCoords[i] = 0;
                for (int j = 0; j < 4; ++j)
                    newCoords[i] += coords[j] * t.Matrix[j, i];
            }
            coords = newCoords;
        }

        public Point3D Transform(Transformation t)
        {
            var p = new Point3D(X, Y, Z);
            p.Apply(t);
            return p;
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            var projected = Transform(projection).NormalizedToDisplay(width, height);
            var x = projected.X;
            var y = projected.Y;
            g.FillEllipse(new SolidBrush(color), 
                (float)(x - POINT_SIZE / 2), (float)(y - POINT_SIZE / 2),
                (float)POINT_SIZE, (float)POINT_SIZE);
        }

        /*
         * Преобразует координаты из ([-1, 1], [-1, 1], [-1, 1]) в ([0, width), [0, height), [-1, 1]).
         */
        public Point3D NormalizedToDisplay(int width, int height)
        {
            var x = (X / K + 1) / 2 * width;
            var y = (-Y / K + 1) / 2 * height;
            return new Point3D(x, y, Z);
        }
    }
    
    class Line : IPrimitive
    {
        private Point3D a;
        private Point3D b;
        private Color color = Color.Black;

        public Point3D A
        {
            get => a;
            set => a = value;
        }
        
        public Point3D B
        {
            get => b;
            set => b = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public Line(Point3D a, Point3D b)
        {
            A = a;
            B = b;
        }
        
        public Line(Point3D a, Point3D b, Color color)
        {
            A = a;
            B = b;
            this.color = color;
        }

        public void Apply(Transformation t)
        {
            A = A.Transform(t);
            B = B.Transform(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            var c = A.Transform(projection).NormalizedToDisplay(width, height);
            var d = B.Transform(projection).NormalizedToDisplay(width, height);
            g.DrawLine(new Pen(color), (float)c.X, (float)c.Y, (float)d.X, (float)d.Y);
        }
    }
    
    class Facet : IPrimitive
    {
        private IList<Point3D> points = new List<Point3D>();
        private Color color = Color.Black;

        public IList<Point3D> Points
        {
            get => points;
            set => points = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public Facet(){}

        public Facet(IList<Point3D> points)
        {
            this.points = points;
        }
        
        public Facet(IList<Point3D> points, Color color)
        {
            this.points = points;
        }

        public void FacetAddPoint(Point3D p)
        {
            points.Add(p);
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            for (int i = 0; i < Points.Count - 1; ++i)
            {
                var line = new Line(Points[i], Points[i + 1], color);
                line.Draw(g, projection, width, height);
            }
            new Line(Points[Points.Count - 1], Points[0], color).Draw(g, projection, width, height);
        }
    }
    
    interface IPolyhedron : IPrimitive
    {
        Point3D Center { get; }
    }
    
    class Tetrahedron : IPolyhedron
    {
        // первые три точки - основание тетраэдра, четвертая точка - его вершина
        private List<Point3D> points = new List<Point3D>();
        private List<Facet> facets = new List<Facet>();

        public List<Point3D> Points => points;
        
        public List<Facet> Facets => facets;

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 4; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 4;
                p.Y /= 4;
                p.Z /= 4;
                return p;
            }
        }

        public Tetrahedron(double size) 
        {
            double h = Math.Sqrt(2.0 / 3.0) * size;
            points = new List<Point3D>();

            points.Add(new Point3D(-size / 2, 0, h / 3));
            points.Add(new Point3D(0, 0, -h * 2 / 3));
            points.Add(new Point3D(size / 2, 0, h / 3));
            points.Add(new Point3D(0, h, 0));

            // Основание тетраэдра
            facets.Add(new Facet(new Point3D[] { points[0], points[1], points[2] }));
            // Левая грань
            facets.Add(new Facet(new Point3D[] { points[1], points[3], points[0] }));
            // Правая грань
            facets.Add(new Facet(new Point3D[] { points[2], points[3], points[1] }));
            // Передняя грань
            facets.Add(new Facet(new Point3D[] { points[0], points[3], points[2] }));
        }

        public Tetrahedron(double size, Color color)
        {
            double h = Math.Sqrt(2.0 / 3.0) * size;
            points = new List<Point3D>();

            points.Add(new Point3D(-size / 2, 0, h / 3, color));
            points.Add(new Point3D(0, 0, -h * 2 / 3, color));
            points.Add(new Point3D(size / 2, 0, h / 3, color));
            points.Add(new Point3D(0, h, 0, color));

            // Основание тетраэдра
            facets.Add(new Facet(new Point3D[] { points[0], points[1], points[2] }, color));
            // Левая грань
            facets.Add(new Facet(new Point3D[] { points[1], points[3], points[0] }, color));
            // Правая грань
            facets.Add(new Facet(new Point3D[] { points[2], points[3], points[1] }, color));
            // Передняя грань
            facets.Add(new Facet(new Point3D[] { points[0], points[3], points[2] }, color));
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            foreach (var facet in Facets)
                facet.Draw(g, projection, width, height);
        }
    }
    
    class Icosahedron : IPolyhedron
    {
        // кол-во вершин = 12
        private List<Point3D> points = new List<Point3D>();

        // кол-во граней = 20
        private List<Facet> facets = new List<Facet>();

        public List<Point3D> Points { get { return points; } }
        public List<Facet> Facets { get { return facets; } }

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 12; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 12;
                p.Y /= 12;
                p.Z /= 12;
                return p;
            }
        }

        public Icosahedron(double size)
        {
            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i)));
                points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10)));
            }

            points.Add(new Point3D(0, R, 0));
            points.Add(new Point3D(0, -R, 0));

            // середина
            for (int i = 0; i < 10; ++i)
                facets.Add(new Facet(new Point3D[] { points[i], points[(i + 1) % 10], points[(i + 2) % 10] }));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                facets.Add(new Facet(new Point3D[] { points[2 * i], points[10], points[(2 * (i + 1)) % 10] }));
                // нижняя часть
                facets.Add(new Facet(new Point3D[] { points[2 * i + 1], points[11], points[(2 * (i + 1) + 1) % 10] }));
            }
        }
        
        public Icosahedron(double size, Color color)
        {
            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i), color));
                points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10), color));
            }

            points.Add(new Point3D(0, R, 0, color));
            points.Add(new Point3D(0, -R, 0, color));

            // середина
            for (int i = 0; i < 10; ++i)
                facets.Add(new Facet(new Point3D[] { points[i], points[(i + 1) % 10], points[(i + 2) % 10] }, color));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                facets.Add(new Facet(new Point3D[] { points[2 * i], points[10], points[(2 * (i + 1)) % 10] }, color));
                // нижняя часть
                facets.Add(new Facet(new Point3D[] { points[2 * i + 1], points[11], points[(2 * (i + 1) + 1) % 10] },
                           color));
            }
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            if (Points.Count != 12) return;
            
            foreach (var facet in Facets)
                facet.Draw(g, projection, width, height);
        }
    }


    class Dodecahedron : IPolyhedron
    {
        // кол-во вершин = 20
        private List<Point3D> points = new List<Point3D>();

        public List<Point3D> Points { get { return points; } }

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 20; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 20;
                p.Y /= 20;
                p.Z /= 20;
                return p;
            }
        }

        public Dodecahedron(double size)
        {
            Icosahedron icosahedron = new Icosahedron(size);

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.Facets[i].Points[0].X
                          + icosahedron.Facets[i].Points[1].X
                          + icosahedron.Facets[i].Points[2].X) / 3;
                double y = (icosahedron.Facets[i].Points[0].Y
                          + icosahedron.Facets[i].Points[1].Y
                          + icosahedron.Facets[i].Points[2].Y) / 3;
                double z = (icosahedron.Facets[i].Points[0].Z
                          + icosahedron.Facets[i].Points[1].Z
                          + icosahedron.Facets[i].Points[2].Z) / 3;
                points.Add(new Point3D(x, y, z));
            }
        }

        public Dodecahedron(double size, Color color)
        {
            Icosahedron icosahedron = new Icosahedron(size);

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.Facets[i].Points[0].X
                          + icosahedron.Facets[i].Points[1].X
                          + icosahedron.Facets[i].Points[2].X) / 3;
                double y = (icosahedron.Facets[i].Points[0].Y
                          + icosahedron.Facets[i].Points[1].Y
                          + icosahedron.Facets[i].Points[2].Y) / 3;
                double z = (icosahedron.Facets[i].Points[0].Z
                          + icosahedron.Facets[i].Points[1].Z
                          + icosahedron.Facets[i].Points[2].Z) / 3;
                points.Add(new Point3D(x, y, z, color));
            }
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            if (Points.Count != 20) return;

            // путь обхода вершин
            int[,] track = new int[2, 30]{
                { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 11, 13, 15, 17, 19},
                { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 12, 14, 16, 18, 10, 13, 15, 17, 19, 11 }};

            for (int i = 0; i < 30; i++)
            {
                Line line1 = new Line(points[track[0, i]], points[track[1, i]]);
                line1.Draw(g, projection, width, height);
            }
        }
    }
}